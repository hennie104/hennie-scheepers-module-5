import 'dart:developer';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:email_validator/email_validator.dart';

import '../utils/utils.dart';

class SignUp extends StatefulWidget {
  final Function() LogInNow;

  const SignUp({
    Key? key,
    required this.LogInNow,
  }) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  // text controllers
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confrimPasswordController = TextEditingController();
  //final _confirmPasswordController = TextEditingController();
  final GlobalKey<NavigatorState> myNavigatorKey = GlobalKey<NavigatorState>();
  final formKey = GlobalKey<FormState>();

  Future signUp() async {
    if (_confrimPasswordController.text.trim() ==
        _passwordController.text.trim()) {
      try {
        await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: _emailController.text.trim(),
            password: _passwordController.text.trim());
      } on FirebaseAuthException catch (e) {
        print(e);
      }

      Utils.showSnackBar(e.toString());
    } else {
      Utils.showSnackBar('The passwords you have entered do not match');
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
        resizeToAvoidBottomInset: false,
        body: Container(
          
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      'https://images.unsplash.com/photo-1585314062340-f1a5a7c9328d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8dGV4dHVyZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60'),
                  fit: BoxFit.cover)),
          child: SafeArea(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(50),
                child: SingleChildScrollView(
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        //Welcome message
                        Text(
                          'Signup',
                          style: GoogleFonts.robotoCondensed(
                              fontSize: 35,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 30),
                        //Textfield for Email
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: TextFormField(
                              controller: _emailController,
                              decoration: const InputDecoration(
                                hintText: 'Email',
                                icon: Icon(Icons.email),
                              ),
                              style: GoogleFonts.robotoCondensed(),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (email) => email != null &&
                                      !EmailValidator.validate(email)
                                  ? 'Enter valid email'
                                  : null,
                            ),
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        const SizedBox(height: 15),

                        //Textfield for password
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: TextFormField(
                              controller: _passwordController,
                              decoration: const InputDecoration(
                                hintText: 'Password',
                                icon: Icon(Icons.password_outlined),
                              ),
                              style: GoogleFonts.robotoCondensed(),
                              obscureText: true,
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (value) =>
                                  value != null && value.length < 6
                                      ? 'Enter min. 6 characters'
                                      : null,
                            ),
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: TextField(
                              controller: _confrimPasswordController,
                              decoration: InputDecoration(
                                hintText: 'Confirm Password',
                                icon: Icon(Icons.password_outlined),
                              ),
                              obscureText: true,
                              style: GoogleFonts.robotoCondensed(),
                            ),
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        const SizedBox(height: 15),
                        //Login button
                        MaterialButton(
                          onPressed: signUp,
                          //color: Colors.green.shade200,
                          child: Text('SignUp',
                              style: GoogleFonts.robotoCondensed()),
                          color: Colors.green.shade200,
                        ),
                        const SizedBox(height: 15),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text('Have an account?'),
                            MaterialButton(
                              onPressed: widget.LogInNow,
                              child: Text(
                                'Log in here',
                                style: TextStyle(color: Colors.blue.shade200),
                              ),
                            ),
                          ],
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
