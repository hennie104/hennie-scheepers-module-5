// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyDxXdYSq4H0n8wPZO1Zo62dO1OyOG88iUo',
    appId: '1:228695094512:web:12e2785e3c5c1e343f7e35',
    messagingSenderId: '228695094512',
    projectId: 'trashaway-990a1',
    authDomain: 'trashaway-990a1.firebaseapp.com',
    storageBucket: 'trashaway-990a1.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyB3xgaJ1AnZXhoZWebV7tkcUDYKiAxDXLc',
    appId: '1:228695094512:android:fb9dd9342a82272e3f7e35',
    messagingSenderId: '228695094512',
    projectId: 'trashaway-990a1',
    storageBucket: 'trashaway-990a1.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyA77YkxC2QmvupcOPvPNVfmTsN-nv5Ptqo',
    appId: '1:228695094512:ios:cc6ac062d4485be23f7e35',
    messagingSenderId: '228695094512',
    projectId: 'trashaway-990a1',
    storageBucket: 'trashaway-990a1.appspot.com',
    iosClientId: '228695094512-tpqlkbpk9bp0dqdvfuh9dsagfemikdh1.apps.googleusercontent.com',
    iosBundleId: 'com.example.trashAway',
  );
}
