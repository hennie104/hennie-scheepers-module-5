import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_fonts/google_fonts.dart';

import 'dashboard.dart';

void main(List<String> args) {
  runApp(const Profile());
}

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            colorScheme: ColorScheme(
                brightness: Brightness.dark,
                primary: Colors.black,
                onPrimary: Colors.white30,
                secondary: Colors.green.shade200,
                onSecondary: Colors.black,
                error: Colors.red.shade200,
                onError: Colors.white,
                background: Colors.transparent,
                onBackground: Colors.black,
                surface: Colors.transparent,
                onSurface: Colors.white),
            inputDecorationTheme: InputDecorationTheme(
              hintStyle: TextStyle(color: Colors.white),
              labelStyle: TextStyle(
                color: Colors.white,
              ),
            ),
            fontFamily: GoogleFonts.robotoCondensed().fontFamily),
        home: Scaffold(
            bottomNavigationBar: BottomAppBar(
              elevation: 0,
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                      icon: Icon(Icons.account_box_rounded),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profile()));
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.home_outlined),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DashBoard()));
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () {},
                    )
                  ],
                ),
              ),
            ),
            resizeToAvoidBottomInset: false,
            body: Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          'https://images.unsplash.com/photo-1585314062340-f1a5a7c9328d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8dGV4dHVyZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60'),
                      fit: BoxFit.cover)),
              child: SafeArea(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(50),
                    child: SingleChildScrollView(
                      child: Form(
                        child: Column(
                          children: [
                            //Welcome message
                            Center(
                              child: Text(
                                'Change personal details',
                                style: GoogleFonts.robotoCondensed(
                                    fontSize: 35,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(height: 30),
                            //Textfield for Email
                            Container(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: TextFormField(
                                  decoration: const InputDecoration(
                                    hintText: 'Email',
                                    icon: Icon(Icons.email),
                                  ),
                                  style: GoogleFonts.robotoCondensed(),
                                ),
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(),
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            const SizedBox(height: 15),

                            //Textfield for password
                            Container(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: TextFormField(
                                  decoration: const InputDecoration(
                                    hintText: 'Password',
                                    icon: Icon(Icons.password_outlined),
                                  ),
                                  style: GoogleFonts.robotoCondensed(),
                                  obscureText: true,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: (value) =>
                                      value != null && value.length < 6
                                          ? 'Enter min. 6 characters'
                                          : null,
                                ),
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(),
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Container(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: 'Confirm Password',
                                    icon: Icon(Icons.password_outlined),
                                  ),
                                  obscureText: true,
                                  style: GoogleFonts.robotoCondensed(),
                                ),
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(),
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            const SizedBox(height: 15),
                            //Login button
                            MaterialButton(
                              onPressed: () {},
                              //color: Colors.green.shade200,
                              child: Text('Save',
                                  style: GoogleFonts.robotoCondensed()),
                              color: Colors.green.shade200,
                            ),
                            const SizedBox(height: 15),
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )));
  }
}
